package crmSystem.dao;

import crmSystem.entity.User;
import org.springframework.data.repository.Repository;

public interface UserRepository extends Repository<User, Integer> {

    User findByUsername(String username);

    void save(User user);
}
