package crmSystem.service;

import crmSystem.entity.User;
import crmSystem.user.CrmUser;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByUsername(String username);

    void save(CrmUser crmUser);
}
