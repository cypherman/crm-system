package crmSystem.dao;

import crmSystem.entity.Role;
import org.springframework.data.repository.Repository;

public interface RoleRepository extends Repository<Role, Integer> {

    Role findByName(String name);
}
