package crmSystem.controller;

import crmSystem.entity.User;
import crmSystem.service.UserService;
import crmSystem.user.CrmUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/registration")
public class RegistrationController {


    @Autowired
    UserService userService;

    @ModelAttribute("roles")
    private Map<String, String> loadRoles() {
        Map<String, String> roles = new HashMap<>();
        roles.put("ROLE_EMPLOYEE", "EMPLOYEE");
        roles.put("ROLE_MANAGER", "MANAGER");
        roles.put("ROLE_ADMIN", "ADMIN");
        return roles;
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor editor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, editor);
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("crmUser", new CrmUser());
        return "registration-form";
    }

    @PostMapping("/register")
    public String process(@Valid @ModelAttribute("crmUser") CrmUser crmUser, BindingResult result, Model model) {
        if (result.hasErrors()) return "registration-form";
        User existing = userService.findByUsername(crmUser.getUsername());
        if (existing != null) {
            model.addAttribute("crmUser", new CrmUser());
            model.addAttribute("registrationError", "Username already exists");
            return "registration-form";
        }
        userService.save(crmUser);
        return "registration-confirmation";
    }
}
