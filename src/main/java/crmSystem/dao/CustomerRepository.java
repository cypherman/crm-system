package crmSystem.dao;

import crmSystem.entity.Customer;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface CustomerRepository extends Repository<Customer, Integer> {

    List<Customer> findAll();

    Customer findById(int id);

    void save(Customer customer);

    void deleteById(int id);

    List<Customer> findAllByFirstNameContaining(String name);
}
