package crmSystem.validation;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {

   private String firstField;
   private String secondField;
   private String message;

   @Override
   public void initialize(FieldMatch constraint) {
      firstField = constraint.first();
      secondField = constraint.second();
      message = constraint.message();
   }

   @Override
   public boolean isValid(Object obj, ConstraintValidatorContext context) {
      boolean valid = true;
      try {
         final Object firstObj = new BeanWrapperImpl(obj).getPropertyValue(firstField);
         final Object secondObj = new BeanWrapperImpl(obj).getPropertyValue(secondField);
         valid = firstObj == null && secondObj == null || firstObj != null && firstObj.equals(secondObj);
      } catch (Exception ignore) {}
      if (!valid) {
         context.buildConstraintViolationWithTemplate(message)
                .addPropertyNode(firstField)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
      }
      return valid;
   }
}
